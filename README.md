# @small-tech/syswide-cas

___Note: this is a fork of the original module being maintained by [Aral Balkan](https://ar.al) of [Small Technology Foundation](https://small-tech.org) as the original company seems to have gone out of business.___

Enables Node.js to use custom Certificate Authorities (CAs) _alongside_ the bundled root CAs.

Until version 7, Node did not support system-wide installed trusted CAs. You could only specify a custom CA via the `ca` option in the `tls` and `https` modules or fallback to using the bundled list of root CAs Node is compiled with.

Starting with version 7, it’s possible to set the `NODE_EXTRA_CA_CERTS` environment variable to a single file containing an additional root CA to trust, however it still does not allow programmatic addition of several directories and files containing root CAs.

This module enables custom CAs to be used _alongside_ the root CAs bundled with Node.

> 💡 syswide-cas will automatically load root CAs from the file `/etc/ssl/ca-node.pem` if it exists.


## Install

```
npm i @small-tech/syswide-cas
```

## Use

> 💡 Import `@small-tech/syswide-cas` before any TLS calls if you use dynamic imports.

```javascript
// Importing syswide-cas automatically loads Certificate Authorities (CAs) from the file _/etc/ssl/ca-node.pem_ if it exists
import syswideCas from '@small-tech/syswide-cas'

// Optionally, load all files from a custom directory.
syswideCas.addCAs('/my/custom/path/to/certs/dir')

// Or multiple directories.
syswideCas.addCAs(['/my/custom/path/to/certs/dir1', '/my/other/path/to/certs/dir2'])

// Optionally, load a file directly.
syswideCas.addCAs('/my/custom/path/to/cert.pem')

// Or multiple files.
syswideCas.addCAs(['/my/custom/path/to/cert1.pem', '/my/other/path/to/cert2.pem'])

import https from 'node:https'
https.get('https://my.custom.domain.com/with/self/signed/cert')
```

## Test

```js
npm -s test
```

### Tests failing? Read this!

If you get the following error when running the tests:

```
✖ FAIL: page with self-signed certificate loads with syswide-cas
```

It’s possible that the Root Certificate Authority has changed on [badssl.com](https://badssl.com).

We use https://self-signed.badssl.com/ for the tests and its Certificate Authority (in PEM format) can be found in _/tests/fixtures/self-signed.badssl.com_.

To update the fixture:

1. In Chromium, visit https://self-signed.badssl.com/
2. Click on the “✖ Not Secure” button in the address bar.
3. Select “Certificate details”
4. Navigate to the Details tab
5. Click the “Export…” button
6. Under “Format” choose “Base64-encoded ASCII, single certificate and save it on top of _/tests/fixtures/self-signed.badssl.com_.

Ensure that the certificate has changed by doing a `git diff` and, if so, run the tests again and they should pass.

Please [open an issue](https://codeberg.org/small-tech/syswide-cas/issues) to let us know the BadSSL certificate has changed and, if you can, please [open a pull request](https://codeberg.org/small-tech/syswide-cas/pulls) to resolve it using the new certificate.

Thanks!

## License

Copyright 2021-present Aral Balkan, Small Technology Foundation.
Copyright 2016 Capriza.

Code released under the [MIT license](LICENSE.md)
